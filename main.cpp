/* 
 * File:   main.cpp
 * Author: Colin Leitner
 * Created on October 23, 2021
 * 
 ***** License:
 * This program is open-source software, but you must alert the original author if you plan to use it, modify it, or copy it, or any part of it.
 * Otherwise, it is under the terms of the GNU General Public License V3. See <http://www.gnu.org/licenses/>
 * This program is distributed WITHOUT ANY WARRANTY whatsoever.
 * 
 ***** About:
 *	TODO could integrate tag unification using NAICS: (Delta only)
 * https://wiki.openstreetmap.org/wiki/NAICS/2017
 *
 ***** How to use:
 *		Be sure "OSM_businesses.json" as well as the city business file you wish to load, is in the working directory.
 * 		Comment out city loading so that only the city you want is active
 */

#include <cstdlib>
#include <string>	// for string class
#include <iostream>
#include <fstream> // for file output
#include <iomanip> //for setw()
#include <unistd.h> //for sleep(), alternative: Windows.h
#include <math.h> //for floor()
#include "dataFunctions.h" //custom functions for loading CSV and JSON data, common to all cities 

using namespace std;


int main(int argc, char* argv[]) {
	cout << "Starting...\n";
	
	cout << setprecision(9);
	bool debug_BusinessOutput = false;
	bool debug_OutMatchToMap = true;
	
	cout << "Loading OSM data...\n";
	
	string* dataNodesExtracted;
	string* dataFeatsExtracted;
	unsigned long nodes_len;
	unsigned long feats_len;
	const unsigned int dataFeatsExtracted_cols = 5;
	const unsigned int dataNodesExtracted_cols = 3;
	
	readOSMtoArrays("OSM_businesses.json", dataNodesExtracted, nodes_len, dataFeatsExtracted, feats_len);
	
	cout << "Loaded " << nodes_len << " nodes, " << feats_len << " features.\n";
	
	//allocate parallel arrays to store contents of file:
	unsigned long* dataNodes_id = new unsigned long[nodes_len];
	double*        dataNodes_lat = new double[nodes_len];
	double*        dataNodes_lon = new double[nodes_len];
	
	unsigned long* dataFeats_id = new unsigned long[feats_len]; // equals "na" if feature is a node
	unsigned long* dataFeats_node = new unsigned long[nodes_len]; //first node of way. Derived from below field
	string*        dataFeats_nodes    = new string[feats_len]; //string of comma-separated whole numbers, converting to long just takes first number
	string*        dataFeats_tags     = new string[feats_len]; //not including name or address
	string*        dataFeats_tagName  = new string[feats_len];
	string*        dataFeats_tagAddr  = new string[feats_len];
	double*        dataFeats_lat      = new double[feats_len]; //not extracted, derived. =node, or first node, TODO geometric mean
	double*        dataFeats_lon      = new double[feats_len]; // ^^
	
	//copy nodes into parallel arrays, converting datatypes:
	for(unsigned long dataNodes_i = 0; dataNodes_i < nodes_len; dataNodes_i++) {
		dataNodes_id[dataNodes_i]  = strtol(dataNodesExtracted[(dataNodes_i*dataNodesExtracted_cols)+0].c_str(), NULL, 10);
		dataNodes_lat[dataNodes_i] =   stod(dataNodesExtracted[(dataNodes_i*dataNodesExtracted_cols)+1].c_str());
		dataNodes_lon[dataNodes_i] =   stod(dataNodesExtracted[(dataNodes_i*dataNodesExtracted_cols)+2].c_str());
	}
	
	//copy features into parallel arrays, converting datatypes:
	long nodeSearchIndex = 0; //for remembering last found location, speeds searches
	for(unsigned long dataFeats_i = 0; dataFeats_i < feats_len; dataFeats_i++) {
		dataFeats_id[dataFeats_i]  = strtol(dataFeatsExtracted[(dataFeats_i*dataFeatsExtracted_cols)+0].c_str(), NULL, 10);
		dataFeats_node[dataFeats_i] = strtol(dataFeatsExtracted[(dataFeats_i*dataFeatsExtracted_cols)+1].c_str(), NULL, 10);
		dataFeats_nodes[dataFeats_i] =      dataFeatsExtracted[(dataFeats_i*dataFeatsExtracted_cols)+1];
		dataFeats_tags[dataFeats_i] =       dataFeatsExtracted[(dataFeats_i*dataFeatsExtracted_cols)+2];
		dataFeats_tagName[dataFeats_i] =       dataFeatsExtracted[(dataFeats_i*dataFeatsExtracted_cols)+3];
		dataFeats_tagAddr[dataFeats_i] =       dataFeatsExtracted[(dataFeats_i*dataFeatsExtracted_cols)+4];
		
		// lookup node and populate lat/lon: TODO parse string of node id's and lookup each to take average
		nodeSearchIndex = lookup(dataNodes_id, dataFeats_node[dataFeats_i], nodes_len, nodeSearchIndex);
		if(nodeSearchIndex == -1) {
			cout << "Error in data: could not find node " << dataFeats_node[dataFeats_i] << ".\n";
		}
		else { //found the node
			dataFeats_lat[dataFeats_i] = dataNodes_lat[nodeSearchIndex];
			dataFeats_lon[dataFeats_i] = dataNodes_lon[nodeSearchIndex];
		}
		
	//	cout << dataFeats_id[dataFeats_i] << "\t";
	//	cout << dataFeats_node[dataFeats_i] << "\t";
//		cout << dataFeats_lat[dataFeats_i] << "/" << dataFeats_lon[dataFeats_i] << "\t";
	//	cout << endl;
	//	cout << dataFeats_nodes[dataFeats_i] << "\t" << endl;
//		cout << dataFeats_tags[dataFeats_i] << endl;
		
//		if(dataFeats_node[dataFeats_i] == 6992381988) {
//			cout << "Found special node at index=" << dataFeats_i << endl;
//			cout << (dataFeats_lat[dataFeats_i] - 49.14412) << " delta lat" << endl;
//			cout << (dataFeats_lon[dataFeats_i] - -123.00195) << " delta lon" << endl;
//		}
	}
	
	delete[] dataNodesExtracted;
	delete[] dataFeatsExtracted;
	
	// no-longer needed after copying lat/lon
	delete[] dataNodes_id;
	delete[] dataNodes_lat;
	delete[] dataNodes_lon;
	
	// no-longer needed after lookup of nodes:
//	delete[] dataFeats_node; //retained for debug purposes
	//delete[] dataFeats_nodes;
	
	cout << "Done converting OSM database." << endl;
	
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	//Setup City data parameters:
	
	if(argc != 2) {
		cout << "Error: You must pass the city name as an argument to this program.\n";
		exit(1);
	}
	string cityName = argv[1];
	string cityFileIn;
	char importantChar = 1;
	int csvData_ColsN = 0;
	string dataBusn_Cols[16]; //NOTE this constant must be greater than the max number of cols of any city
	string dataRootDir = "/home/colin/Archives/BigData/officialData/";
	
	if(cityName == "delta") {
		cityFileIn = dataRootDir + "delta/Delta_BUS_DIRECTORY.csv";
		importantChar = '1'; //minimum criteria for export
		csvData_ColsN = 6;
	//	string tempCols[] = {"Name", "BUSINESS_TYPE", "HNumb", "Street", "PostCode", "Lat", "Lon", "Interesting?"};
		string tempCols[] = {"TRADE_NAME", "BUSINESS_TYPE", "ADDRESS", "Long", "Lat", "TRADE_NAICS_CODE"};
		copy(begin(tempCols), end(tempCols), begin(dataBusn_Cols)); //reassign array
	}
	else if(cityName == "surrey") {
		cityFileIn = dataRootDir + "surrey/restaurants.csv";
		importantChar = '1'; //minimum criteria for export
		csvData_ColsN = 5;
		string tempCols[] = {"NAME", "FACTYPE", "PHYSICALADDRESS", "LATITUDE", "LONGITUDE"};
		copy(begin(tempCols), end(tempCols), begin(dataBusn_Cols)); //reassign array
	}
	else if(cityName == "burnaby") {
		cityFileIn = dataRootDir + "burnaby/burnaby_businesses.csv";
		importantChar = '1'; //minimum criteria for export
		csvData_ColsN = 10;
		string tempCols[] = {"TRADE_NAME", "LICENCE_TYPE_NAME", "STREET", "HOUSE", "UNIT", "Y", "X", "COVERS_FROM", "COVERS_TO", "ACCOUNT_NAME"};
		copy(begin(tempCols), end(tempCols), begin(dataBusn_Cols)); //reassign array
	}
	else if(cityName == "toronto") {
		cityFileIn = dataRootDir + "toronto/business.licences.csv";
		importantChar = '4'; //minimum criteria for export
		csvData_ColsN = 9;
		string tempCols[] = {"\"Operating Name\"", "\"Category\"", "\"Licence Address Line 1\"", "\"Licence Address Line 2\"", "\"Licence Address Line 3\"", "\"Business Phone\"", "\"Client Name\"", "\"Issued\"", "\"Cancel Date\""};
		copy(begin(tempCols), end(tempCols), begin(dataBusn_Cols)); //reassign array
	}
	else {
		cout << "Error: City not supported yet." << endl;
		exit(1);
	}
	
	unsigned long dataBusn_Len; //value will be written by readCSVtoArray()
	string* dataCSVExtracted = readCSVtoArray(cityFileIn, dataBusn_Cols, dataBusn_Len, csvData_ColsN);
	
	//allocate parallel arrays to store contents of file:
	string* dataBusn_Name   = new string[dataBusn_Len];
	string* dataBusn_Type   = new string[dataBusn_Len];
	string* dataBusn_HNumb  = new string[dataBusn_Len];
	string* dataBusn_Street = new string[dataBusn_Len];
	string* dataBusn_Unit   = new string[dataBusn_Len];
	string* dataBusn_Postal = new string[dataBusn_Len];
	double* dataBusn_Lat    = new double[dataBusn_Len];
	double* dataBusn_Lon    = new double[dataBusn_Len];
	char*	dataBusn_Wanted =   new char[dataBusn_Len];
	string* dataBusn_Phone  = new string[dataBusn_Len];
	string* dataBusn_Oper   = new string[dataBusn_Len];
	string* dataBusn_Start  = new string[dataBusn_Len];
	string* dataBusn_End    = new string[dataBusn_Len];
	string* dataBusn_City   = new string[dataBusn_Len];
	string* dataBusn_OsmEq	= new string[dataBusn_Len];
	
	
	//copy from 2D array into parallel arrays, converting datatypes:
	if(cityName == "delta") {
	// .xlsx processing steps:
	//		concatenate name & lat & lon and remove duplicates
	//		replace all \n with "," or other unique char
	
	for(unsigned long dataBusn_i = 0; dataBusn_i < dataBusn_Len; dataBusn_i++) {
		int col = 0; //be sure parer lines below are in same order as dataBusn_Cols[]
	//	cout << "Longitude: " << dataCSVExtracted[(dataBusn_i*csvData_ColsN)+6] << endl;
		dataBusn_Name[dataBusn_i]   = titleCase(dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col]); col++;
		dataBusn_Type[dataBusn_i]   = dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col]; col++;
		string addrTemp = dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col]; col++; 
		dataBusn_HNumb[dataBusn_i] = addrTemp.substr(0, addrTemp.find_first_of(' ', 0));
		int nextDash = dataBusn_HNumb[dataBusn_i].find_first_of('-', 0);
		if(nextDash > -1) {
			dataBusn_Unit[dataBusn_i] = dataBusn_HNumb[dataBusn_i].substr(0, nextDash);
			dataBusn_HNumb[dataBusn_i] = dataBusn_HNumb[dataBusn_i].substr(1 + nextDash);
		}
		else { //if no dash, then no unit #
			dataBusn_Unit[dataBusn_i] = "";
		}
		dataBusn_Street[dataBusn_i] = titleCase(addrTemp.substr(1 + addrTemp.find_first_of(' ', 0), addrTemp.find_first_of(',', 0)));
		if(addrTemp.at(addrTemp.length() - 4) == ' ') { //look for the space in the postal code
			dataBusn_Postal[dataBusn_i] = addrTemp.substr(addrTemp.length() - 7);
		}
		else {
			dataBusn_Postal[dataBusn_i] = "";
		}
		//NOTE that city of Delta's header file has Lat/Long swapped!!!
	//	cout << dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col] << endl;
		if(dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col] != "0") {
		dataBusn_Lat[dataBusn_i] = (stod(dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col].c_str()) * 0.0000089963318844) + 0.172613569; col++; // see spreadsheet .ods for conversion calcs
		dataBusn_Lon[dataBusn_i] = (stod(dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col].c_str()) * 0.0000137478949379) - 129.8732834 - 0.0003; col++;
		}
		else {
			dataBusn_Lat[dataBusn_i] = 0;
			dataBusn_Lon[dataBusn_i] = 0;
		}
		dataBusn_Oper[dataBusn_i] = dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col]; col++; //TODO temp
		
		// Initialize this in case the type is not recognized. For translating dataBusn_Type[dataBusn_i] into an OSM key/value pair
		string osmEq = "";
		char w = '0';
		// Determine wanted value:
		string type = dataBusn_Type[dataBusn_i];
			 if(type == "Bed & Breakfast")								{ w = '3'; osmEq="tourism=guest_house$guest_house=bed_and_breakfast";}
		else if(type == "All other out-patient care centres")			{ w = '2';}
		else if(type == "All other personal services")					{ w = '2';}
		else if(type == "Artist Studio, Gallery")						{ w = '2'; osmEq="tourism=gallery";}
		else if(type == "Arts, entertainment & recreation / All other amusement & recreation industries")		{ w = '2';}
		else if(type == "Arts, entertainment & recreation / Dance companies")									{ w = '2'; osmEq="leisure=dance";}
		else if(type == "Arts, entertainment & recreation / Independent artists, performers and writers")		{ w = '2';}
		else if(type == "Arts, entertainment & recreation / Other performing arts companies")					{ w = '2';}
		else if(type == "Banquet Halls with Own Catering Staff")		{ w = '2'; osmEq="amenity=events_venue";}
		else if(type == "BEER, WINE & LIQUOR STORES")					{ w = '3'; osmEq="shop=alcohol";}
		else if(type == "Boarding Horses (except Racehorses)")			{ w = '2'; osmEq="leisure=horse_riding";}
		else if(type == "Car Detailers/Washes")							{ w = '2'; osmEq="amenity=car_wash";}
		else if(type == "Coin-operated laundries & dry cleaners")		{ w = '2'; osmEq="shop=laundry$self_service=yes";}
		else if(type == "Convenience Stores")							{ w = '3'; osmEq="shop=convenience";}
		else if(type == "Donation Bins")								{ w = '3';}
		else if(type == "Drinking places (alcoholic beverages)")		{ w = '3'; osmEq="amenity=bar";}
		else if(type == "Dry cleaning & laundry serv. (exc. coin-op.)")	{ w = '2';}
		else if(type == "Educational services / All other schools & instruction")				{ w = '2';}
		else if(type == "Educational services / Athletic instruction")							{ w = '2';}
		else if(type == "Educational services / Community colleges and C.E.G.E.P.s")			{ w = '2';}
		else if(type == "Educational services / Educational support services")					{ w = '2';}
		else if(type == "Educational services / Fine arts schools")								{ w = '2';}
		else if(type == "Educational services / Language schools")								{ w = '2';}
		else if(type == "Educational services / Professional & mgmt. development training")		{ w = '2';}
		else if(type == "Educational services / Technical & trade schools")						{ w = '2';}
		else if(type == "Frozen Yogurt Sales")													{ w = '3'; osmEq="amenity=ice_cream";}
		else if(type == "Films, Motion Picture Production")										{ w = '3';}
		else if(type == "Finance & insurance / Banking")										{ w = '2'; osmEq="amenity=bank";}
		else if(type == "Finance & insurance / Investment banking & securities dealing")		{ w = '2'; osmEq="office=financial_advisor";}
		else if(type == "Finance & insurance / Local credit unions")	{ w = '2'; osmEq="amenity=bank";}
		else if(type == "Fitness & recreational sports centres")		{ w = '3'; osmEq="leisure=fitness_centre";}
		else if(type == "Footwear & leather goods repair")				{ w = '2'; osmEq="shop=shoe_repair";}
		else if(type == "Full-service restaurants, ")					{ w = '3'; osmEq="amenity=restaurant";}
		else if(type == "General rental centres")						{ w = '3'; osmEq="shop=yes";}
		else if(type == "Golf courses & country clubs")					{ w = '2'; osmEq="leisure=golf_course";}
		else if(type == "Health care / Offices of dentists")			{ w = '2'; osmEq="healthcare=dentist";}
		else if(type == "Health care / Offices of optometrists")		{ w = '2'; osmEq="healthcare=optometrist";}
		else if(type == "Hotel (except casino hotel) & motel")			{ w = '3'; osmEq="tourism=hotel";}
		else if(type == "Kennels, Pet Boarding.")						{ w = '2'; osmEq="amenity=animal_boarding";}
		else if(type == "Limited-Service Eating Places")				{ w = '3'; osmEq="amenity=fast_food";}
		else if(type == "Local messengers & local delivery")			{ w = '2';}
		else if(type == "Lawn and garden equipment and supplies stores"){ w = '2'; osmEq="shop=garden_centre";}
		else if(type == "Lottery Centres")								{ w = '2'; osmEq="shop=lottery";}
		else if(type == "Marinas")										{ w = '3'; osmEq="leisure=marina";}
		else if(type == "Martial Arts Instruction, Camps or Schools.")	{ w = '2'; osmEq="amenity=dojo";}
		else if(type == "Mineral Water, Retail")						{ w = '2'; osmEq="shop=water";}
		else if(type == "Mobile Food Services")							{ w = '3';}
		else if(type == "Personal Services / Barber Shops")				{ w = '2'; osmEq="shop=hairdresser";}
		else if(type == "Personal Services / Beauty Salons")			{ w = '2'; osmEq="shop=hairdresser";}
		else if(type == "Pet care (exc. veterinary) services")			{ w = '2';}
		else if(type == "Pharmacies Retail")							{ w = '3'; osmEq="healthcare=pharmacy$amenity=pharmacy";}
		else if(type == "Photographic services")						{ w = '2';}
		else if(type == "Public Cafeteria")								{ w = '3'; osmEq="amenity=pub";}
		else if(type == "Religious organizations")						{ w = '1'; osmEq="amenity=place_of_worship";}
		else if(type == "Retail trade / All other general merchandise stores")		{ w = '2'; osmEq="shop=yes";}
		else if(type == "Retail trade / All other misc. store retailers")			{ w = '2'; osmEq="shop=yes";}
		else if(type == "Retail trade / Art dealers")								{ w = '2'; osmEq="shop=art";}
		else if(type == "Retail trade / Automotive parts & accessories stores")		{ w = '2'; osmEq="shop=car_parts";}
		else if(type == "Retail trade / Beer, wine & liquor stores")				{ w = '2'; osmEq="shop=alcohol";}
		else if(type == "Retail trade / Book stores & news dealers")				{ w = '2'; osmEq="shop=books";}
		else if(type == "Retail trade / Children's & infants' clothing stores")		{ w = '2'; osmEq="shop=clothes";}
		else if(type == "Retail trade / Clothing accessories stores")				{ w = '2'; osmEq="shop=fashion_accessories";}
		else if(type == "Retail trade / Convenience stores")						{ w = '3'; osmEq="shop=convenience";}
		else if(type == "Retail trade / Department stores")							{ w = '2'; osmEq="shop=yes";}
		else if(type == "Retail trade / Electronic and appliance stores")			{ w = '2'; osmEq="shop=appliance";}
		else if(type == "Retail trade / Fish & seafood markets")					{ w = '2'; osmEq="shop=seafood";}
		else if(type == "Retail trade / Floor covering stores")						{ w = '2'; osmEq="shop=flooring";}
		else if(type == "Retail trade / Florists")									{ w = '2'; osmEq="shop=florist";}
		else if(type == "Retail trade / Fruit & vegetable markets")					{ w = '2'; osmEq="shop=greengrocer";}
		else if(type == "Retail trade / Furniture stores")							{ w = '2'; osmEq="shop=furniture";}
		else if(type == "Retail trade / Gasoline stations with convenience stores")	{ w = '2'; osmEq="amenity=fuel";}
		else if(type == "Retail trade / Gift, novelty & souvenir stores")			{ w = '2'; osmEq="shop=gift";}
		else if(type == "Retail trade / Grocery (exc. convenience) stores")			{ w = '2'; osmEq="shop=supermarket";}
		else if(type == "Retail trade / Hardware stores")							{ w = '2'; osmEq="shop=hardware";}
		else if(type == "Retail trade / Hobby, toy & game stores")					{ w = '2'; osmEq="shop=toys";}
		else if(type == "Retail trade / Home centres")								{ w = '2'; osmEq="shop=houseware";}
		else if(type == "Retail trade / Jewellery stores")							{ w = '2'; osmEq="shop=jewelry";}
		else if(type == "Retail trade / Luggage & leather goods stores")			{ w = '2'; osmEq="shop=bag";}
		else if(type == "Retail trade / Meat markets")								{ w = '2'; osmEq="shop=butcher";}
		else if(type == "Retail trade / Men's clothing stores")						{ w = '2'; osmEq="shop=clothes";}
		else if(type == "Retail trade / Motorcycle, boat & other mv dealers")		{ w = '2'; osmEq="shop=boat$shop=car$shop$shop=motorcycle";}
		else if(type == "Retail trade / Nursery & garden centres")					{ w = '2'; osmEq="shop=garden_centre";}
		else if(type == "Retail trade / Office supplies & stationery & gift stores"){ w = '2'; osmEq="shop=stationery";}
		else if(type == "Retail trade / Other building material dealers")			{ w = '2'; osmEq="shop=trade";}
		else if(type == "Retail trade / Other clothing stores")						{ w = '2'; osmEq="shop=clothes";}
		else if(type == "Retail trade / Other direct selling establishments")		{ w = '2'; osmEq="shop=yes";}
		else if(type == "Retail trade / Other health & personal care stores")		{ w = '3'; osmEq="shop=yes";}
		else if(type == "Retail trade / Other home furnishings stores")				{ w = '2'; osmEq="shop=interior_decoration";}
		else if(type == "Retail trade / Other specialty food stores")				{ w = '2'; osmEq="shop=yes";}
		else if(type == "Retail trade / Paint & wallpaper stores")					{ w = '2'; osmEq="shop=paint";}
		else if(type == "Retail trade / Pet & pet supplies stores")					{ w = '2'; osmEq="shop=pet";}
		else if(type == "Retail trade / Pharmacies & drug stores")					{ w = '2'; osmEq="healthcare=pharmacy$amenity=pharmacy";}
		else if(type == "Retail trade / Recreational vehicle dealers")				{ w = '2'; osmEq="shop=yes";}
		else if(type == "Retail trade / Shoe stores")								{ w = '2'; osmEq="shop=shoes";}
		else if(type == "Retail trade / Sporting goods stores")						{ w = '2'; osmEq="shop=sports";}
		else if(type == "Retail trade / Tire dealers")								{ w = '2'; osmEq="shop=tyres";}
		else if(type == "Retail trade / Used car dealers")							{ w = '2'; osmEq="shop=car$second_hand=only";}
		else if(type == "Retail trade / Vending machine operators")					{ w = '2'; osmEq="shop=yes";}
		else if(type == "Retail trade / Women's clothing stores")					{ w = '2'; osmEq="shop=clothes$clothes=women";}
		else if(type == "Seasonal / Christmas Tree Sales")							{ w = '2'; osmEq="shop=garden_centre";}
		else if(type == "Seasonal / Southlands Farmers Market")						{ w = '2'; osmEq="shop=farm";}
		else if(type == "Seasonal / Temporary Retail Kiosks")						{ w = '2'; osmEq="shop=yes";}
		else if(type == "Tattoo Parlours")											{ w = '2'; osmEq="shop=tattoo";}
		else if(type == "Technical and Trade Schools")								{ w = '2'; osmEq="amenity=college";}
		else if(type == "Truck, utility trailer & RV rental & leasing")				{ w = '2'; osmEq="shop=caravan";}
		else if(type == "Utilities / Electric power transmission, control & dist.")	{ w = '2'; osmEq="office=energy_supplier";}
		else if(type == "Utilities / Natural gas distribution")						{ w = '2'; osmEq="office=energy_supplier";}
		else if(type == "Veterinary services")										{ w = '2'; osmEq="amenity=veterinary";}
		else if(type == "Wine Making Supplies Retail")								{ w = '2'; osmEq="craft=winery";}
		else if(type == "Wineries")													{ w = '2'; osmEq="craft=winery";}
		else { w = '1'; }
		
		// copy temp value into array: this is done just to reduce code length
		dataBusn_Wanted[dataBusn_i] = w;
		dataBusn_OsmEq[dataBusn_i] = osmEq;
		
		
		//cout << dataBusn_Lon[dataBusn_i] << endl;
	}
	}
	
	else if(cityName == "surrey") {
	for(unsigned long dataBusn_i = 0; dataBusn_i < dataBusn_Len; dataBusn_i++) {
		int col = 0; //be sure parer lines below are in same order as dataBusn_Cols[]
	//	cout << "Longitude: " << dataCSVExtracted[(dataBusn_i*csvData_ColsN)+6] << endl;
		dataBusn_Name[dataBusn_i]   = dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col]; col++;
		dataBusn_Type[dataBusn_i]   = ""; col++; //the current dataset always is resaurant, just ignore
		dataBusn_HNumb[dataBusn_i]  = dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col].substr(0, dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col].find_first_of(' ', 0)); //split column upto first space char
		dataBusn_Street[dataBusn_i] = dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col].substr(1+ dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col].find_first_of(' ', 0)); col++; //split column following first space char
		dataBusn_Postal[dataBusn_i] = ""; // NA from surrey
		dataBusn_Unit[dataBusn_i] = ""; // NA from surrey
		dataBusn_Lat[dataBusn_i] = stod(dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col].c_str()); col++;
		dataBusn_Lon[dataBusn_i] = stod(dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col].c_str()); col++;
		dataBusn_Wanted[dataBusn_i] = '3'; //all items in this dataset are considered important
		dataBusn_OsmEq[dataBusn_i] = "amenity=restaurant";
		
		//cout << dataBusn_Name[dataBusn_i] << "," << dataBusn_Lon[dataBusn_i] << endl;
	}
	}
	
	else if(cityName == "burnaby") {	
	for(unsigned long dataBusn_i = 0; dataBusn_i < dataBusn_Len; dataBusn_i++) {
		int col = 0; //be sure parer lines below are in same order as dataBusn_Cols[]
	//	cout << "Longitude: " << dataCSVExtracted[(dataBusn_i*csvData_ColsN)+6] << endl;
		dataBusn_Name[dataBusn_i]   = titleCase(dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col]); col++;
		dataBusn_Type[dataBusn_i]   = dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col]; col++;
		dataBusn_Street[dataBusn_i] = titleCase(dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col]); col++;
		dataBusn_HNumb[dataBusn_i]  = dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col]; col++;
		dataBusn_Unit[dataBusn_i]	= dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col]; col++;
		dataBusn_Postal[dataBusn_i] = ""; // NA from Burnaby
		dataBusn_Lat[dataBusn_i] = (stod(dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col].c_str()) + 15211.7) / 111075.05; col++; // see spreadsheet .ods for conversion calcs
		dataBusn_Lon[dataBusn_i] = (stod(dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col].c_str()) - 9428400.0) / 72588.3; col++;
		dataBusn_Start[dataBusn_i]  = dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col]; col++;
		dataBusn_End[dataBusn_i]    = dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col]; col++;
		dataBusn_Oper[dataBusn_i]   = titleCase(dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col]); col++;
		
		// Initialize this in case the type is not recognized. For translating dataBusn_Type[dataBusn_i] into an OSM key/value pair
		string osmEq = "";
		char w = '0';
		// Determine wanted value:
		string type = dataBusn_Type[dataBusn_i];
			 if(type == "COIN AND/OR NOTE OPERATED MACHINES")			{ w = '2'; osmEq="amenity=vending_machine";}
		else if(type == "FILM LOCATION")								{ w = '3'; }
		else if(type == "PEDDLER - FOOD")								{ w = '3'; osmEq="amenity=fast_food";}
		else if(type == "ADULT SERVICES - BODY RUB PREMISES")			{ w = '2'; osmEq="shop=massage";}
		else if(type == "ARCADE")										{ w = '3'; osmEq="leisure=amusement_arcade";}
		else if(type == "BANK")											{ w = '1'; osmEq="amenity=bank";}
		else if(type == "BEVERAGE CONTAINER RETURN CENTRE")				{ w = '2'; osmEq="amenity=recycling$recycling_type=centre";}
		else if(type == "BOWLING ALLEY")								{ w = '2'; osmEq="leisure=bowling_alley";}
		else if(type == "CART")											{ w = '1'; }
		else if(type == "CREDIT UNION")									{ w = '1'; osmEq="amenity=bank";}
		else if(type == "CURLING RINK / ICE RINK")						{ w = '2'; osmEq="leisure=ice_rink";}
		else if(type == "CYBER CENTRES")								{ w = '3'; osmEq="amenity=cafe$internet_access=yes";}
		else if(type == "HEALTH SERVICES - DENTIST/DENTAL SERV.")		{ w = '1'; osmEq="healthcare=dentist";}
		else if(type == "GAS SERVICE STATION")							{ w = '1'; osmEq="amenity=fuel";}
		else if(type == "HOTEL/MOTEL/AUTO COURT - PER SUITE")			{ w = '3'; osmEq="tourism=hotel";}
		else if(type == "INCINERATOR")									{ w = '3'; }
		else if(type == "JUNK DEALER")									{ w = '1'; osmEq="industrial=scrap_yard";}
		else if(type == "LIQUOR ESTABLISHMENT - CLASS A HOTEL")			{ w = '3'; }
		else if(type == "LIQUOR ESTABLISHMENT - CLASS D NEIGHBOURHOOD PUB"){w='3'; osmEq="amenity=pub";}
		else if(type == "LIQUOR ESTABLISHMENT - BEER & WINE STORE")		{ w = '1'; osmEq="shop=alcohol";}
		else if(type == "NOT FOR PROFIT")								{ w = '2'; osmEq="office=association";} //this is cryptic, but what ID editor uses
		else if(type == "NURSERY")										{ w = '1'; osmEq="landuse=plant_nursery";}
		else if(type == "PARKING LOT")									{ w = '1'; osmEq="amenity=parking";}
		else if(type == "OIL STORAGE PLANT & DISTRIBUTION")				{ w = '2'; osmEq="industrial=oil$landuse=industrial";}
		else if(type == "OIL REFINERY")									{ w = '2'; osmEq="industrial=oil$landuse=industrial";}
		else if(type == "PRIVATE SCHOOL")								{ w = '1'; osmEq="amenity=school$operator:type=private";}
		else if(type == "PUBLIC HALL")									{ w = '2'; osmEq="amenity=pub";} //unsure how Burnaby defines this
		else if(type == "RESTAURANT - TAKE OUT")						{ w = '3'; osmEq="amenity=fast_food";}
		else if(type == "RESTAURANT 1 - 10 SEATS")						{ w = '3'; osmEq="amenity=restaurant";}
		else if(type == "RESTAURANT 11 - 50 SEATS")						{ w = '3'; osmEq="amenity=restaurant";}
		else if(type == "RESTAURANT 51 - 150 SEATS")					{ w = '3'; osmEq="amenity=restaurant";}
		else if(type == "RESTAURANT 151 & OVER SEATS")					{ w = '3'; osmEq="amenity=restaurant";}
		else if(type == "RETAIL SALE, RENTAL & REPAIR")					{ w = '1'; osmEq="shop=yes";}
		else if(type == "RETAIL TRADER - FOOD 1 - 10 PERSONS")			{ w = '3'; osmEq="shop=convenience";}
		else if(type == "RETAIL TRADER - FOOD 11 - 50 PERSONS")			{ w = '3'; osmEq="shop=supermarket";}
		else if(type == "RETAIL TRADER - FOOD 51+ PERSONS")				{ w = '3'; osmEq="shop=supermarket";}
		else if(type == "RETAIL TRADER - GENERAL 1 - 10 PERSONS")		{ w = '2'; osmEq="shop=yes";}
		else if(type == "RETAIL TRADER - GENERAL 11 - 50 PERSONS")		{ w = '1'; osmEq="shop=yes";}
		else if(type == "RETAIL TRADER - GENERAL 51+ PERSONS")			{ w = '1'; osmEq="shop=yes";}
		else if(type == "SECOND HAND DEALER")							{ w = '2'; osmEq="shop=second_hand";}
		else if(type == "VENDING MACHINE (ANY TYPE) USING A CREDIT CARD"){ w= '2'; osmEq="amenity=vending_machine";}
		else if(type == "VENDING MACHINE (AMUSEMENT OR RECREATION)")	{ w = '3'; osmEq="attraction=kiddie_ride";} // unsure about this
		else if(type == "VETERINARIAN")									{ w = '1'; osmEq="amenity=veterinary";}
		else if(type == "THEATRE - INDOOR")								{ w = '3'; osmEq="amenity=theatre$amenity=cinema";} //unsure how Burnaby defines this
		else if(type == "TELEPHONE COMMUNICATIONS FACILITY 151+ PERSONS"){ w= '1'; osmEq="telecom=exchange";}
		else if(type == "TELEPHONE COMMUNICATION FACILITY 1 - 50 PERSONS"){ w='1'; osmEq="telecom=exchange";}
		else if(type == "TRAILER CAMP/COURT")							{ w = '3'; osmEq="residential=trailer_park";}
		// The above categories comprise only about 10% of all Burnaby businesses
		else { w = '0'; }
		
		// copy temp value into array: this is done just to reduce code length
		dataBusn_Wanted[dataBusn_i] = w;
		dataBusn_OsmEq[dataBusn_i] = osmEq;
		
	//	cout << dataBusn_Name[dataBusn_i] << "," << dataBusn_Lon[dataBusn_i] << endl;
		
		//status:
		if(dataBusn_i % 10000 == 0) {
			cout << "Parsing business index " << dataBusn_i << "..." << endl;
		}
	}
	}
	
	else if(cityName == "toronto") {
	// Toronto only: Load three split address array files:
	
	//WARNING loading the address file between loading and parsing the business data (as is arranged currently) uses unnessesary memory consumption.
	
	//NOTE: requirements for input address files: each line must start with at least 1 tab so that quotes are not detected as cell delimiters
	// all files must be exact parallel arrays (same length, they share a length variable here)
	// each line must end with a comma, and each street name must be quoted (so that there are 2 chars after the name on every line)
	
	unsigned long dataAddr_Len; //value will be written by readCSVtoArray()
	string dataAddrSt_Cols[] = {"Street"};
	string* dataAddrSt_Extracted = readCSVtoArray(dataRootDir+"toronto/streets.txt", dataAddrSt_Cols, dataAddr_Len, 1);
	string* dataAddr_Street = new string[dataAddr_Len]; //allocate parallel arrays to store contents of file
	//parse and copy to array:
	for(unsigned long dataAddr_i = 0; dataAddr_i < dataAddr_Len; dataAddr_i++) {
		dataAddr_Street[dataAddr_i] = dataAddrSt_Extracted[dataAddr_i].substr(16, dataAddrSt_Extracted[dataAddr_i].length()-17);
	//	cout << dataAddr_Street[dataAddr_i] << endl;
	}
	delete[] dataAddrSt_Extracted;
	
	string dataAddrHN_Cols[] = {"HouseNumber"};
	string* dataAddrHN_Extracted = readCSVtoArray(dataRootDir+"toronto/hNumbs.txt", dataAddrHN_Cols, dataAddr_Len, 1);
	string* dataAddr_hNumb = new string[dataAddr_Len]; //allocate parallel arrays to store contents of file
	//parse and copy to array:
	for(unsigned long dataAddr_i = 0; dataAddr_i < dataAddr_Len; dataAddr_i++) {
		dataAddr_hNumb[dataAddr_i] = dataAddrHN_Extracted[dataAddr_i].substr(15);
	//	cout << dataAddr_hNumb[dataAddr_i] << endl;
	}
	delete[] dataAddrHN_Extracted;
	
	string dataAddrPt_Cols[] = {"LonString", "LatString"};
	string* dataAddrPt_Extracted = readCSVtoArray(dataRootDir+"toronto/points.txt", dataAddrPt_Cols, dataAddr_Len, 2);
	double* dataAddr_Lat = new double[dataAddr_Len]; //allocate parallel arrays to store contents of file
	double* dataAddr_Lon = new double[dataAddr_Len];
	//parse and copy to array:
	for(unsigned long dataAddr_i = 0; dataAddr_i < dataAddr_Len; dataAddr_i++) {
		dataAddr_Lon[dataAddr_i] = stod(dataAddrPt_Extracted[(dataAddr_i*2)+0].substr(22).c_str());
		dataAddr_Lat[dataAddr_i] = stod(dataAddrPt_Extracted[(dataAddr_i*2)+1].substr(1).c_str()); //trailing bracket ignored by stod()
	//	cout << dataAddr_Lat[dataAddr_i] << "," << dataAddr_Lon[dataAddr_i] << endl;
	}
	delete[] dataAddrPt_Extracted;
	
	cout << "Loaded " << dataAddr_Len << " street addresses.\n";
	
	
	
	for(unsigned long dataBusn_i = 0; dataBusn_i < dataBusn_Len; dataBusn_i++) {
		int col = 0; //be sure parer lines below are in same order as dataBusn_Cols[]
	//	cout << "Longitude: " << dataCSVExtracted[(dataBusn_i*csvData_ColsN)+6] << endl;
		dataBusn_Name[dataBusn_i]   = titleCase(dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col]); col++;
		dataBusn_Type[dataBusn_i]   = dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col]; col++;
		dataBusn_HNumb[dataBusn_i]  = dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col].substr(0, dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col].find_first_of(' ', 0)); //split column upto first space char
		string streetTmp 			= dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col].substr(1+ dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col].find_first_of(' ', 0)); //split column following first space char
		int nextComma = streetTmp.find_first_of(',', 0); //not sure why this doesn't work inside if(). Returns special string position datatype?
		if(nextComma > -1) { //unit number sometimes appears after a comma, but can also be a letter after the housenumber
			dataBusn_Street[dataBusn_i] = streetTmp.substr(0, streetTmp.find_first_of(',', 0));
			dataBusn_Unit[dataBusn_i] = streetTmp.substr(1 + streetTmp.find_first_of(',', 0));
			while(dataBusn_Unit[dataBusn_i].front() == ' ') { dataBusn_Unit[dataBusn_i] = dataBusn_Unit[dataBusn_i].substr(1); } // while is used in case there are multiple spaces
			while(dataBusn_Unit[dataBusn_i].front() == '#') { dataBusn_Unit[dataBusn_i] = dataBusn_Unit[dataBusn_i].substr(1); } // remove "#" char
		}
		else {
			dataBusn_Street[dataBusn_i] = streetTmp;
			dataBusn_Unit[dataBusn_i] = "";
		}
		//check if any unit letter is part of housenumber. This overrides the "#", since it's attached to number > unit
		if((dataBusn_HNumb[dataBusn_i].back() >= 'A') && (dataBusn_HNumb[dataBusn_i].back() <= 'Z')) {
			dataBusn_Unit[dataBusn_i] = dataBusn_HNumb[dataBusn_i].back();
			dataBusn_HNumb[dataBusn_i].pop_back();
		}
		int nextDash = dataBusn_HNumb[dataBusn_i].find_first_of('-', 0);
		if(nextDash > -1) { //a dash indicates a range. Only take the first number:
			dataBusn_HNumb[dataBusn_i] = dataBusn_HNumb[dataBusn_i].substr(0, nextDash);
		}
		
		col++;
		dataBusn_City[dataBusn_i]   = dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col]; col++;
		dataBusn_Postal[dataBusn_i] = dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col]; col++;
		dataBusn_Phone[dataBusn_i]  = dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col]; col++;
		dataBusn_Oper[dataBusn_i]   = titleCase(dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col]); col++;
		dataBusn_Start[dataBusn_i]  = dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col]; col++;
		dataBusn_End[dataBusn_i]    = dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col]; col++;
		
		//clean text:
		while(dataBusn_End[dataBusn_i].back() == '"') { dataBusn_End[dataBusn_i].pop_back(); }
		while(dataBusn_End[dataBusn_i].front() == '"') { dataBusn_End[dataBusn_i].erase(0,1); } //clip first byte
		
		// initialize to a special number that signals to be ignored during output
		dataBusn_Lat[dataBusn_i] = 0; //43.58; // stod(dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col].c_str()); col++;
		dataBusn_Lon[dataBusn_i] = 0; //-79.00; // stod(dataCSVExtracted[(dataBusn_i*csvData_ColsN)+col].c_str()); col++;
		
		// Initialize this in case the type is not recognized. For translating dataBusn_Type[dataBusn_i] into an OSM key/value pair
		string osmEq = "";
		char w = '0';
		string type = dataBusn_Type[dataBusn_i];
			 if(type == "PLACE OF AMUSEMENT") 		{ w = '3'; } // 0.2% in toronto
		else if(type == "BOWLING HOUSE") 			{ w = '3'; }
		else if(type == "BOATS FOR HIRE") 			{ w = '1'; }
		else if(type == "CARNIVAL") 				{ w = '3'; }
		else if(type == "CIRCUS")					{ w = '3'; }
		else if(type == "SMOKE SHOP")				{ w = '2'; }
		else if(type == "RETAIL STORE (FOOD)")		{ w = '3'; osmEq="shop=supermarket";} // 17% in toronto
		else if(type == "LAUNDRY")					{ w = '1'; }
		else if(type == "THEATRE")					{ w = '2'; }
		else if(type == "PRECIOUS METAL SHOP")		{ w = '1'; }
		else if(type == "PAWN SHOP")				{ w = '1'; }
		else if(type == "PET SHOP")					{ w = '1'; }
		else if(type == "PUBLIC GARAGE")			{ w = '1'; } // 9% in toronto
		else if(type == "PUBLIC HALL")				{ w = '4'; osmEq="amenity=pub";} // 0.2% in toronto
		else if(type == "EATING ESTABLISHMENT")		{ w = '4'; osmEq="amenity=restaurant";} // 25% in toronto
		else if(type == "SECOND HAND SHOP")			{ w = '2'; osmEq="shop=second_hand";}
		else if(type == "SECOND HAND SALVAGE SHOP")	{ w = '2'; }
		else if(type == "SWIMMING POOL")			{ w = '2'; }
		else if(type == "ENTERTAINMENT ESTABLISHMENT/NIGHTCLUB") { w = '2'; } // 0.1% in toronto
		else if(type == "CLOTHING DROP BOX LOCATION PERMIT") { w = '2'; }
		else if(type == "SIDEWALK VENDING")			{ w = '2'; } // 0.1% in toronto
		else if(type == "SIDEWALK CAFE")			{ w = '2'; osmEq="amenity=cafe";} // 1.2% in toronto
		else if(type == "COLLECTOR OF SECOND HAND GOODS") { w = '1'; osmEq="shop=pawnbroker";}
	//	else if(type == "NON-MOTORIZED REFRESHMENT VEHICLE OWNER"){ w = '1'; }
	//	else if(type == "MOTORIZED REFRESHMENT VEHICLE OWNER"){ w = '1'; }
		else { dataBusn_Wanted[dataBusn_i] = '0'; }
		
		// copy temp value into array: this is done just to reduce code length
		dataBusn_Wanted[dataBusn_i] = w;
		dataBusn_OsmEq[dataBusn_i] = osmEq;
		
	//	cout << dataBusn_Wanted[dataBusn_i] << "," << dataBusn_Name[dataBusn_i] << endl;
		/*
		// lookup lat and lon in address table: (NOTE: can't use custom lookup() because need to check each match further)
		bool foundAddress = false;
		unsigned long dataAddr_i = 0;
		while(!foundAddress && (dataAddr_i < dataAddr_Len)) {
			if(dataBusn_HNumb[dataBusn_i] == dataAddr_hNumb[dataAddr_i]) {
				//capitalize strings for case-insensitive comparison:
				string dataBusn_StreetTmp = dataBusn_Street[dataBusn_i];
				for(int c = 0; c < dataBusn_StreetTmp.length(); c += 1) {
					dataBusn_StreetTmp.at(c) = toupper(dataBusn_StreetTmp.at(c));
				}
				string dataAddr_StreetTmp = dataAddr_Street[dataAddr_i];
				for(int c = 0; c < dataAddr_StreetTmp.length(); c += 1) {
					dataAddr_StreetTmp.at(c) = toupper(dataAddr_StreetTmp.at(c));
				}
				
			//	cout << "Checking " << dataBusn_StreetTmp << " with " << dataAddr_StreetTmp << endl;
				if(dataBusn_StreetTmp == dataAddr_StreetTmp) {
					foundAddress = true;
					dataBusn_Lat[dataBusn_i] = dataAddr_Lat[dataAddr_i];
					dataBusn_Lon[dataBusn_i] = dataAddr_Lon[dataAddr_i];
			//		cout << "Matched Address " << dataAddr_hNumb[dataAddr_i] << " " << dataAddr_Street[dataAddr_i] << endl;
					
					// copy regular case address back over, Toronto address dataset has correct capitalization:
					dataBusn_Street[dataBusn_i] = dataAddr_Street[dataAddr_i];
				}
			}
			dataAddr_i += 1;
		}
		if(!foundAddress) {
			cout << "Could not locate address " << dataBusn_City[dataBusn_i] << " " << dataBusn_HNumb[dataBusn_i] << " " << dataBusn_Street[dataBusn_i] << endl;
		}
		*/
		//status:
		if(dataBusn_i % 10000 == 0) {
			cout << "Parsing business index " << dataBusn_i << "..." << endl;
		}
	}
	
	delete[] dataAddr_Lat;
	delete[] dataAddr_Lon;
	delete[] dataAddr_Street;
	delete[] dataAddr_hNumb;
	}
	else {} //already handled by city setup
	
	delete[] dataCSVExtracted;
	cout << "Loaded " << dataBusn_Len << " records in CSV file.\n";
	cout << endl;
	
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	// compare each entry to each in other table:
	//TODO add high-score array of 3-element vectors? Use structs to group id+score.
	// This will allow choosing the best match when several are available, as well as reverse check to find old/spam businesses on OSM
	
	time_t now = time(0); // current date/time based on current system
	tm *ltm = localtime(&now);
	unsigned long long todayDateCode = 0; //initialize to "time zero"
//	todayDateCode += (ltm->tm_year) * 100000000; //years since 1900
	todayDateCode += (1 + ltm->tm_mon) * 1000000; //0-indexed month of year
	todayDateCode += (ltm->tm_mday) * 10000; //1-indexed day of month
	todayDateCode += (ltm->tm_hour) * 100; // 24hour
	todayDateCode += (ltm->tm_min) * 1; //min
	
	unsigned long long thisYear = (ltm->tm_year) + 1900;
	cout << "Current year is " << thisYear << endl;
	
	ofstream fileOut;
	fileOut.open("outputs/"+cityName+".json", (ios::out)); //"ios::app" is not used here, so that content is replaced
	if(!fileOut.is_open()) {
		cout << "\nCould not create output file in working directory.\n";
		exit(1);
	}
	fileOut << setprecision(5) << fixed << "{\"LastUpdated\":" << todayDateCode << ", \"Note\":\"beta.\",\"Places\":[" << endl;
	bool firstEntry = true;
	
	long statMatched = 0;
	long statMarkedImportant = 0;
	long statMatchedImportant = 0;
	
	// Compare entries in each table to each other:
	for(unsigned long dataBusn_i = 0; dataBusn_i < dataBusn_Len; dataBusn_i++) {
		//status update:
		if(dataBusn_i % 5000 == 0) {
			cout << "Matching business index " << dataBusn_i << "..." << endl;
		}
		if(dataBusn_Lon[dataBusn_i] < 50) { //skip places with no position. NOTE -50 is for North America!
		//TODO above check should be -50 !!!!
		
		cout << dataBusn_Oper[dataBusn_i] << "|" << dataBusn_Start[dataBusn_i] << "|" << dataBusn_End[dataBusn_i] << "|" << dataBusn_End[dataBusn_i].length() << endl;
		unsigned long long yearEndBusn = 0;
		if(dataBusn_End[dataBusn_i] != "") {
		if(dataBusn_End[dataBusn_i].length() == 11) {
			yearEndBusn = stol(dataBusn_End[dataBusn_i].substr(dataBusn_End[dataBusn_i].length()-1)); //strip last 4 chars to get year
		}
		else { cout << "Warning: invalid date format " << dataBusn_End[dataBusn_i] << " Line " << dataBusn_i << endl; }
		}
		else { yearEndBusn = thisYear; }
			
		long OSM_Matches = 0;
		for(unsigned long dataFeats_i = 0; dataFeats_i < feats_len; dataFeats_i++) {
			//filter for proximity:
			if(abs(dataBusn_Lat[dataBusn_i] - dataFeats_lat[dataFeats_i]) < 0.006) {
				if(abs(dataBusn_Lon[dataBusn_i] - dataFeats_lon[dataFeats_i]) < 0.006) { // 0.006 = 450m longitude at 49degN
					
				//	cout << dataBusn_Name[dataBusn_i] << " is close to " << dataFeats_tagName[dataFeats_i] << endl;
					
					//compare fuzzy similarity between two names:
					double matchFrac = fuzzyCompare(dataBusn_Name[dataBusn_i], dataFeats_tagName[dataFeats_i]);
					
					if(matchFrac > 0.03) { // considerable match
						if(matchFrac > 0.20) { //very likely a valid match (these constants just from experience)
				//			cout << "\033[32;1m";
							statMatched += 1;
							OSM_Matches += 1;
							if(dataBusn_Wanted[dataBusn_i] >= importantChar) {
								statMatchedImportant += 1;
							}
							
							if(debug_OutMatchToMap) { //NOTE copied from usual output branch below, but some data replaced with OSM
								if(!firstEntry) { fileOut << ",\n"; } //surpress comma
								fileOut << "{\"gp\":" << "-1"; //override the "wanted group"
								fileOut << ",\"lat\":" << dataFeats_lat[dataFeats_i] << ",\"lon\":" << dataFeats_lon[dataFeats_i];
								fileOut << ",\"desc\":\"" << dataBusn_Type[dataBusn_i] << "\",\"tags\":\"";
								if(dataBusn_OsmEq[dataBusn_i] != "") { fileOut << dataBusn_OsmEq[dataBusn_i] << "$"; }
								fileOut << "name=" << dataBusn_Name[dataBusn_i] << "$";
								fileOut << "addr:housenumber=" << dataBusn_HNumb[dataBusn_i] << "$";
								fileOut << "addr:street=" << dataBusn_Street[dataBusn_i];
								if(dataBusn_Postal[dataBusn_i] != "") { fileOut << "$addr:postcode=" << dataBusn_Postal[dataBusn_i]; }
								if(dataBusn_Unit[dataBusn_i] != "") { fileOut << "$addr:unit=" << dataBusn_Unit[dataBusn_i]; }
								if(dataBusn_Oper[dataBusn_i] != "") { fileOut << "$operator=" << dataBusn_Oper[dataBusn_i]; }
								if(dataBusn_Phone[dataBusn_i] != "") { fileOut << "$phone=" << dataBusn_Phone[dataBusn_i]; }
								if(dataBusn_End[dataBusn_i] != "") { fileOut << "$end_date=" << dataBusn_End[dataBusn_i]; }
								fileOut << "\"}";
								firstEntry = false;
							}
						}
						
						if(debug_BusinessOutput) {
							cout << dataBusn_Name[dataBusn_i] << " ~ ";
							cout << dataFeats_tagName[dataFeats_i];
							cout << "\033[0m";
							cout << "\tMatch=" << matchFrac <<","<< "\t";
						//	cout << abs(dataBusn_Lat[dataBusn_i] - dataFeats_lat[dataFeats_i]) << " ";
							cout << endl;
							cout << " https://www.openstreetmap.org/?mlat=" << dataBusn_Lat[dataBusn_i] << "&mlon=" << dataBusn_Lon[dataBusn_i] << "#map=18/" << dataBusn_Lat[dataBusn_i] << "/" << dataBusn_Lon[dataBusn_i] << endl;
						}
					}
					
					
					// TODO organize match results for this dataBusn_*[dataBusn_i] into array here
					// TODO (check if greater than current best, shift over, or append)
				}
			}
			
		}
		
		if ((OSM_Matches == 0) && (dataBusn_Wanted[dataBusn_i] >= importantChar)) {
			if(debug_BusinessOutput) {
				cout << "\033[31;1m";
				cout << "Not on OSM: " << dataBusn_Name[dataBusn_i] << "";
				cout << "\033[0m";
				cout << " https://www.openstreetmap.org/?mlat=" << dataBusn_Lat[dataBusn_i] << "&mlon=" << dataBusn_Lon[dataBusn_i] << "#map=18/" << dataBusn_Lat[dataBusn_i] << "/" << dataBusn_Lon[dataBusn_i];
				cout << endl;
			}
			
			//write unmatched city business to JSON: the "$" is not special, just an obscure char that the javascript web map parses.
			if(!firstEntry) { fileOut << ",\n"; } //surpress comma
			fileOut << "{\"gp\":" << dataBusn_Wanted[dataBusn_i];
			fileOut << ",\"lat\":" << dataBusn_Lat[dataBusn_i] << ",\"lon\":" << dataBusn_Lon[dataBusn_i];
			fileOut << ",\"desc\":\"" << dataBusn_Type[dataBusn_i] << "\",\"tags\":\"";
			if(dataBusn_OsmEq[dataBusn_i] != "") { fileOut << dataBusn_OsmEq[dataBusn_i] << "$"; }
			fileOut << "name=" << dataBusn_Name[dataBusn_i] << "$";
			fileOut << "addr:housenumber=" << dataBusn_HNumb[dataBusn_i] << "$";
			fileOut << "addr:street=" << dataBusn_Street[dataBusn_i];
			if(dataBusn_Postal[dataBusn_i] != "") { fileOut << "$addr:postcode=" << dataBusn_Postal[dataBusn_i]; }
			if(dataBusn_Unit[dataBusn_i] != "") { fileOut << "$addr:unit=" << dataBusn_Unit[dataBusn_i]; }
			if(dataBusn_Oper[dataBusn_i] != "") { fileOut << "$operator=" << dataBusn_Oper[dataBusn_i]; }
			if(dataBusn_Phone[dataBusn_i] != "") { fileOut << "$phone=" << dataBusn_Phone[dataBusn_i]; }
			if(dataBusn_End[dataBusn_i] != "") { fileOut << "$end_date=" << dataBusn_End[dataBusn_i]; }
			fileOut << "\"}";
			firstEntry = false;
		}
		}
		else {
			cout << "\033[31;1m" << "No geo-location for " << dataBusn_Name[dataBusn_i] << "\033[0m" << endl;
		}
		
		if(dataBusn_Wanted[dataBusn_i] >= importantChar) {
			statMarkedImportant += 1;
		}
	}
	
	fileOut << "\n]}"; // browser is NOT ok with trailing comma in JSON!!!
	fileOut.close();
	
	cout << endl << "Of " << dataBusn_Len << " city businesses, found " << statMatched << " matches with score >0.2 between datasets." << endl;
	cout << statMatchedImportant << " / " << statMarkedImportant << " = " << ((double)statMatchedImportant/statMarkedImportant)*100 << " % important businesses matched." << endl;
	cout << ((double)statMatched/dataBusn_Len)*100 << " % of all city data is on OSM." << endl;
	
	//cleanup all dynamic arrays:
	//(nodes already deleted)
	delete[] dataFeats_id;
	delete[] dataFeats_node;
	delete[] dataFeats_nodes;
	delete[] dataFeats_tags;
	delete[] dataFeats_tagName;
	delete[] dataFeats_tagAddr;
	delete[] dataFeats_lat;
	delete[] dataFeats_lon;
	
	delete[] dataBusn_Name;
	delete[] dataBusn_Type;
	delete[] dataBusn_HNumb;
	delete[] dataBusn_Street;
	delete[] dataBusn_Postal;
	delete[] dataBusn_Lat;
	delete[] dataBusn_Lon;
	delete[] dataBusn_Wanted;
	delete[] dataBusn_Unit;
	
	return 0;
}

