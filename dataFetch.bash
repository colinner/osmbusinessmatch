#!/bin/bash

# Get OSM data for the region:
# TODO can add --quiet to wget
# TODO remove no-clobber options when this script is added to cron

dataRootDir="$HOME/Archives/BigData/officialData/"

#Delta only:
#bbox="(49.00297,-123.20000,49.19269,-122.88997);"
#Delta + Surrey + Richmond:
bbox="(49.00297,-123.20000,49.21600,-122.67940);"
#Burnaby:
#bbox="(49.18,-123.023,49.295,-122.8925);"
#Toronto:
#bbox="(43.586,-79.638,43.849,-79.120);"

#Comment out lines to exclude:
queryTags="nw[shop]$bbox%20"
queryTags+="nw[amenity~%22childcare|vending|food|cafe|resta|pub|veterinary|animal_boarding|clinic|bank|fuel|recycling%22]$bbox%20"
queryTags+="nw[office]$bbox%20"
queryTags+="nw[craft]$bbox%20"
queryTags+="nw[man_made=works]$bbox%20"
queryTags+="nw[tourism~%22guest_house|motel|hotel|gallery%22]$bbox%20"
queryTags+="nw[leisure~%22horse_riding|sports_centre|fitness_centre|dance|marina%22]$bbox%20"
queryTags+="nw[landuse=plant_nursery]$bbox%20"
queryTags+="nw[building=warehouse]$bbox%20"
queryTags+="nw[healthcare]$bbox"

#Food only: (for Toronto)
#queryTags="nw[shop]$bbox%20nw[amenity~%22vending|food|cafe|resta|pub|fuel%22]$bbox%20"

# Send query:
#wget --referer=ltnr.ca --output-document=OSM_businesses.json "https://overpass-api.de/api/interpreter?data=%5Bout:json%5D;($queryTags);(._;>;);out%20qt;"



# Get official business listings:

# Delta: (updated weekly)
# https://www.delta.ca/your-government/municipal-information/open-data/open-data-government-license
# https://www.delta.ca/your-government/municipal-information/open-data-catalogue
wget --no-clobber --force-directories --referer=ltnr.ca --output-document=$dataRootDir"delta/Delta_BUS_DIRECTORY.xlsx" "https://calls.delta.ca/opendata-statistics/folder/Business_Directory/file/Delta_BUS_DIRECTORY.xlsx"
#TODO the .xlsx file needs to be cleaned (see steps in existing file) and converted to CSV!


# Surrey: (updated monthly)
# https://data.surrey.ca/pages/open-government-licence-surrey
# https://data.surrey.ca/group/business-and-economy
# check for no existing files newer than 0.5 day ago:
if [ "$(find $dataRootDir"surrey/restaurants.csv" -atime -0.5 )" == "" ]
then
	mv $dataRootDir"surrey/restaurants.csv" $dataRootDir"surrey/restaurants_OLD.csv"
	wget --no-clobber --force-directories --referer=ltnr.ca --output-document=$dataRootDir"surrey/restaurants.csv" "https://data.surrey.ca/dataset/3c8cb648-0e80-4659-9078-ef4917b90ffb/resource/0e5d04a2-be9b-40fe-8de2-e88362ea916b/download/restaurants.csv"
else
	echo "Surrey city data still recent. Source updated montly."
fi

# Burnaby: (updated every 4 months???)
# https://data.burnaby.ca/datasets/business-licences
# https://data.burnaby.ca/pages/open-government-licence
wget --no-clobber --force-directories --referer=ltnr.ca --output-document=$dataRootDir"burnaby/burnaby_businesses.csv" "https://opendata.arcgis.com/api/v3/datasets/d4de3afbeffd4728a75a219e9d0f07a5_17/downloads/data?format=csv&spatialRefId=4326"


# Toronto: (updated daily)
# https://open.toronto.ca/open-data-license/
# https://open.toronto.ca/dataset/municipal-licensing-and-standards-business-licences-and-permits/
if [ "$(find $dataRootDir"toronto/business.licences.csv" -atime -0.5 )" == "" ]
then
	mv $dataRootDir"toronto/business.licences.csv" $dataRootDir"toronto/business.licences_OLD.csv"
	wget --no-clobber --force-directories --referer=ltnr.ca --output-document=$dataRootDir"toronto/business.licences.csv" "https://ckan0.cf.opendata.inter.prod-toronto.ca/download_resource/173e493c-31da-48db-85dd-60b2b6ce8f66"
else
	echo "Toronto city data still recent. Source updated daily."
fi

# Should not be required regularly:
wget --no-clobber --force-directories --referer=ltnr.ca --output-document=$dataRootDir"toronto/address_point_wgs84_geojson.zip" "https://ckan0.cf.opendata.inter.prod-toronto.ca/download_resource/185e02d0-e8ff-4a96-80c1-e723c2c21d11"
unzip -u -o $dataRootDir"toronto/address_point_wgs84_geojson.zip" -d $dataRootDir"toronto/"
${dataRootDir}toronto/splitJson.sh
