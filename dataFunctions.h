/*
 * 
 * File:   dataFunctions.h
 * Author: Colin Leitner
 * Created on November 20, 2021
 * 
 ***** License:
 * This program is open-source software, but you must alert the original author if you plan to use it, modify it, or copy it, or any part of it.
 * Otherwise, it is under the terms of the GNU General Public License V3. See <http://www.gnu.org/licenses/>
 * This program is distributed WITHOUT ANY WARRANTY whatsoever.
 * 
 */

#ifndef DATAFUNCTIONS_H
#define DATAFUNCTIONS_H

#include <cstdlib>
#include <string>	// for string class
#include <iostream>

using namespace std;


//perform a fuzzy match between two names, where the number of similar consecutive chars is scored
double fuzzyCompare(string strA, string strB) {
	long score = 0; //score for each pattern = (matched words) / (fuzzy match max)
	long possibleScore = 0;
	int maxSearchLen = 9; // max is 8, chosen as long enough for most words
	if(strB.length() < maxSearchLen) { maxSearchLen = strB.length(); }
	if(strA.length() < maxSearchLen) { maxSearchLen = strA.length(); }
	for(int searchLen = 3; searchLen < maxSearchLen; searchLen++) {
		//capitalize strings for case-insensitive comparison:
		string strAUpper = strA;
		for(int c = 0; c < strAUpper.length(); c += 1) {
			strAUpper.at(c) = toupper(strAUpper.at(c));
		}
		string strBUpper = strB;
		for(int c = 0; c < strBUpper.length(); c += 1) {
			strBUpper.at(c) = toupper(strBUpper.at(c));
		}
		
	//	cout << strBUpper << endl;
		
		for(int aPos = 0; aPos <= (strAUpper.length() - searchLen); aPos++) {
		for(int bPos = 0; bPos <= (strBUpper.length() - searchLen); bPos++) {
			if(strAUpper.substr(aPos,searchLen) == strBUpper.substr(bPos,searchLen)) {
				score += searchLen;
			}
			possibleScore += searchLen;
		}
		}
	}
	long rawScore = score;
	//	if(dataBusn_Name[dataFeats_i].length() > 0) { //avoid divide by zero
	//		score = round(score * 10 / dataBusn_Name[dataFeats_i].length()); //ensures patterns with less unmatched text score better. OSM names tend to be shorter and succinct, but city may have more details.
	//	}
	double matchFrac = 0;
	if(possibleScore > 0) { //avoid divide by zero
		matchFrac = (double)score / (pow(possibleScore, 0.77653) * 0.85); //length of both strings is considered in the possibleScore var
	}
	
	/*
	// new algorithm for matching, looks for contiguous substrings of any length that can start at any place in either input string
	long score = 0;

	int matchedSeq = 0;
	for(int aPos = 0; aPos < dataBusn_Name[dataBusn_i].length(); aPos++) {
		for(int bPos = 0; bPos < dataFeats_tagName[dataFeats_i].length(); bPos++) {
			
		}
	}
	long rawScore = score;
	*/
	
	return matchFrac;
}


// Make a string title-case so that each word starts with a capital, but lowercase otherwise:
string titleCase(string inputString) {
	string newString = inputString;
	
	for(int i = 0; i < newString.length(); i += 1) {
		if(i==0) {
			newString.at(i) = toupper(newString.at(i)); // this should not affect numbers or other non-letter chars
		}
		else if((newString.at(i-1) == ' ') || (newString.at(i-1) == '-') || (newString.at(i-1) == '&')) {
			newString.at(i) = toupper(newString.at(i));
		}
		else {
			newString.at(i) = tolower(newString.at(i));
		}
	}
	
	return newString;
}

//function that can search a given array of any type
//starts at 'hint' position, search wraps to top and continues down if not found
//returns index of found data, or -1 if not found
template <typename arrayDataType>
long lookup(arrayDataType arrayIn[], arrayDataType findValue, unsigned long arraySize, long i_StartAt) {
	//search down from hint, then up from hint if not found?? Depends on how data is sorted
	//having two separate search loops is faster than one multi-purpose loop
	long i = i_StartAt;
	while((arrayIn[i] != findValue) && (i < arraySize)) {
		i++;
	}
	if(i == arraySize) { //didn't find value after hint
		//searching backwards will prevent returning all possible matches in arrayIn[], it will get stuck on last match and not loop back to index 0
		
		//re-start searching from 0 to i_StartAt:
		i = 0; //don't subtract 1 because while loop needs to check at this index
		cout << "Search wrapped while looking for " << findValue << endl;
		while((arrayIn[i] != findValue) && (i < i_StartAt)) {
			i++;
		}
		if(i == i_StartAt) {
			return -1; //entire array searched, not found
		}
		else {
			return i;
		}
	}
	else {
		return i;
	}
}


//function that completely handles parsing of any CSV file, and loads select columns into a psuedo 2D array [a*b], not a real [][] because it requires constant size apparently
//calling code must: provide pointer to store dataSize, and delete[] returned array after use
//file requirements:
//any whitespace between commas and at end of line is captured EVEN IN HEADERS
//trailing comma in header okay (causes extra field, slower)
//extra trailing commas in records okay (ignored)
//truncated records okay, causes empty strings for each missing cell
//linebreak after last record optional, no effect. However, any chars after will count as more records


//unsigned long readCSVtoArrays(string fileName, string colNamesIn[], string arrayOut[], unsigned int outColsN) {
//Originally, the plan was to return the size and store the array pointer to a referenced varaible, as above
//This did not work however, possibly because it's not possible for calling code to create a dynamic array, and then the function sets the size
string* readCSVtoArray(string fileName, string colNamesIn[], unsigned long &dataSize, unsigned int outColsN) {
	cout << "Loading CSV file: " << fileName << endl;
	
	ifstream dataFileIn(fileName);
	if (dataFileIn.fail()) {
		cout << "\033[31mLoading failed. Exiting.\033[0m\n";
		exit(1);
	}
	string currentLine;
	int totalCols = 0;
	
	//dataFileTrips.unsetf(std::ios_base::skipws); //new lines will be skipped by default?? This was suggested by stackoverflow answer, but seems to make no difference
	
	//determine column to array mapping:
	int colIisArrayX[1024]; //index/elements of this array == columns in CSV input, values of elements == index of corresponding colNamesIn[]
	//initialize array:
	for(int i=0; i<1024; i+=1) {
		colIisArrayX[i] = -1;
	}
	getline(dataFileIn, currentLine); //load header string
	cout << "Header: " << currentLine << endl;
	
	//cleanup:
	while((currentLine.back()=='\r') || (currentLine.back()=='\n') || (currentLine.back()=='\f')) { //any newlines would be considered part of the last field
		currentLine.pop_back();
	}
	while(((long)currentLine.front() < ' ') || ((long)currentLine.front() > '~')) { //lately, has been weird chars appended to csv file, prevents field match later
		cout << "WARNING: removing non-ASCII character from start of header: #" << (long)currentLine.front() << endl;
		currentLine.erase(0,1); //clip first byte
	}
	
	cout << "Mapping requested fields:\n";
	int col = 0;
	int colFoundCounter = 0;
	while(currentLine.length() > 0) { //while there are still chars left (they are erased as parsed)
		string thisColName;
		int nextComma = currentLine.find_first_of(',', 0); //for some reason, this doesn't work if put directly in the if()
		if(nextComma > -1) {
			thisColName	= currentLine.substr(0, nextComma);
			currentLine.erase(0, nextComma + 1); //move to next field
		}
		else {
			thisColName = currentLine;
			currentLine = ""; //exit while loop
		}
		
	//	cout << "Checking if '" << thisColName << "' field requested..." << endl;
	//	cout << "'" << thisColName.front() << "' '" << thisColName.back() << "' \t";
	//	cout << endl;
		
		for(int i=0; i<outColsN; i+=1) {
		//	cout << "length=" << colNamesIn[i].length() << " \t";
		//	cout << "'" << colNamesIn[i].front() << "' '" << colNamesIn[i].back() << "' \t";
		//	cout << "cmp=" << thisColName.compare(colNamesIn[i]) << "\t";
			if(thisColName == colNamesIn[i]) {
				colIisArrayX[col] = i;
				cout << i << "-" << col << "\t" << thisColName << endl;
				colFoundCounter += 1;
			}
		}
	//	cout << endl;
		col += 1;
	}
	totalCols = col;
	cout << "File contains " << totalCols << " fields" << endl;
	
	//check that all requested columns were found:
	if(colFoundCounter < outColsN) {
		cout << "Error: " << (outColsN-colFoundCounter) << " field headers could not be found in file.\n";
		cout << endl;
		exit(1);
	}
	
	//determine number of rows:
	dataSize = 0;
	//header has already been skipped above
	while (getline(dataFileIn, currentLine)) {
		dataSize++;
	}
	cout << "File contains " << dataSize << " records" << endl;
	
	//temporary array for loading data into:
	string* arrayOut = new string[dataSize * outColsN]; //real 2D array requires constant size???? :(
	
	//begin reading file, storing data into arrayTmp:
	dataFileIn.clear(); //reset "eof" flag to allow seeking
	dataFileIn.seekg(0, dataFileIn.beg); //go back to start
	getline(dataFileIn, currentLine); //skip file header
	
	unsigned long dataIndex = 0;
	while (dataIndex < dataSize) {
		getline(dataFileIn, currentLine);
		while(currentLine.back()=='\r') { //if Windows carriage return char
			currentLine.pop_back();
		}
		col = 0;
		while(col < totalCols) {
			bool quotedCell = false;
			if(currentLine.length() > 0) {
			if(currentLine.at(0) == '"') {
				quotedCell = true;
				currentLine.erase(0, 1); //erase leading quote
			}}
			
			string cellContent;
			int nextComma = currentLine.find_first_of(',', 0); //for some reason, this doesn't work if put directly in the if()
			if(nextComma > -1) { //if no match is found, it returns string::npos
				if(quotedCell) {
					cellContent = currentLine.substr(0, currentLine.find_first_of('"', 0)); //extract upto trailing quote
					currentLine.erase(0, currentLine.find_first_of('"', 0) + 2); //move to next field
				}
				else {
					cellContent = currentLine.substr(0, nextComma); //extract upto next comma
					currentLine.erase(0, nextComma + 1); //move to next field
				}
			}
			else {
				cellContent = currentLine;
				//this should be the last column if no more commas follow
			}
		//	cout << "'" << cellContent << "'\t";
			if(colIisArrayX[col] != -1) {
				arrayOut[(dataIndex * outColsN) + colIisArrayX[col]] = cellContent;
	//	cout << "Stored=" << arrayOut[(dataIndex * outColsN) + colIisArrayX[col]] << "\t";
			}
			
			col++;
		}
	//	cout << endl;
		dataIndex++;
	}
	
	dataFileIn.close();
	cout << "Done loading all records in file.\n";
	
	return arrayOut; //return pointer to newly created array
	//number of records found in file, not including header, is already stored in variable passed-in by ref
}



//reads an OSM JSON file (from overpass query) and converts into two arrays of:
// points (id,lat,lon)
// features (id, lat, lon, string of all nodes, string of all tags) where lat/lon is from first node
// nodes are usually first in file (may not be in future) and so cannot lookup nodes of a way until entire file is parsed
//		(that is why this function does not do this)
// Design flaw: the whole idea of temp storage of strings to an approx-size array is not needed because it is possible 
//  to determine the exact size needed by scanning the file for keywords ("node","way")
// Reusibility is not needed either (columns are always the same datatype)
// so storing directly to parallel arrays would be more efficient
void readOSMtoArrays(string fileName, string*& nodeArrayOut, unsigned long &nodesSize, string*& featArrayOut, unsigned long &featsSize) {
	cout << "Loading OSM JSON file: " << fileName << endl;
	
	const unsigned int feats_cols = 5;
	const unsigned int nodes_cols = 3;
	
	ifstream dataFileIn(fileName);
	if (dataFileIn.fail()) {
		cout << "\033[31mCan't find file. Exiting.\033[0m\n";
		exit(1);
	}
	string currentLine;
	
	//dataFileTrips.unsetf(std::ios_base::skipws); //new lines will be skipped by default?? This was suggested by stackoverflow answer, but seems to make no difference
	
	//determine number of lines:
	unsigned long fileLines = 0;
	while (getline(dataFileIn, currentLine)) {
		fileLines++;
	}
	nodesSize = fileLines / 6; // an empty node element in the JSON has minimum 6 lines: 2 brackets, type, id, lat, lon
	featsSize = fileLines / 9; // a feature has additional: "'tags': {", closing bracket, and at least one tag
	
	cout << "File contains " << fileLines << " lines, < " << nodesSize << " nodes, < " << featsSize << " features.\n";
	
	//temporary array for loading data into:
	string* nodesArrayOut = new string[nodesSize * nodes_cols]; //real 2D array requires constant size???? :(
	string* featsArrayOut = new string[featsSize * feats_cols];
	
	//begin reading file:
	dataFileIn.clear(); //reset "eof" flag to allow seeking
	dataFileIn.seekg(0, dataFileIn.beg); //go back to start
	unsigned long fileLine = 0;
	unsigned long wayCount = 0;
	
	unsigned long node_i = 0;
	unsigned long feat_i = 0;
	while (getline(dataFileIn, currentLine)) {
		fileLine += 1; //first line is line 1
		//erase any unpredictable whitespace chars from end:
		while(currentLine.back()=='\r') { //if Windows carriage return char TODO check other linebreaks also
			currentLine.pop_back();
		}
		while(((currentLine.front()==' ') || (currentLine.front()=='\t')) && (currentLine.size() > 1)) { //trim any tabulation
			currentLine.erase(0, 1);
		}
		
		if(currentLine == "\"type\": \"node\",") { //this line is the start of a node. Begin looking on following lines for fields of this node
			int fieldsFound = 0;
			bool thisObjDone = false;
			while(!thisObjDone) {
				getline(dataFileIn, currentLine);
				fileLine += 1;
				while(((currentLine.front()==' ') || (currentLine.front()=='\t')) && (currentLine.size() > 1)) { //trim any tabulation
					currentLine.erase(0, 1);
				}
			//	cout << currentLine << endl;
				
				if(currentLine.substr(0,5) == "\"id\":") {
					fieldsFound += 1;
					if(currentLine.back()==',') { currentLine.pop_back(); }
					nodesArrayOut[(node_i*nodes_cols)+0] = currentLine.substr(6); //from 5 to rest of string
				}
				else if(currentLine.substr(0,6) == "\"lat\":") {
					fieldsFound += 1;
					if(currentLine.back()==',') { currentLine.pop_back(); }
					nodesArrayOut[(node_i*nodes_cols)+1] = currentLine.substr(6);
				}
				else if(currentLine.substr(0,6) == "\"lon\":") {
					fieldsFound += 1;
					if(currentLine.back()==',') { currentLine.pop_back(); }
					nodesArrayOut[(node_i*nodes_cols)+2] = currentLine.substr(6);
				}
				else if(currentLine == "\"tags\": {") {
				//	fieldsFound += 1; don't increment- this is not a critical tag
					
					//this node is also a feature:
					featsArrayOut[(feat_i*feats_cols)+0] = "na"; //basically, this is the way/relation OSM id, which does not apply to nodes
					featsArrayOut[(feat_i*feats_cols)+1] = nodesArrayOut[(node_i*nodes_cols)+0]; //node id for finding lat/lon
					featsArrayOut[(feat_i*feats_cols)+2] = ""; //initialize tag string to empty
					featsArrayOut[(feat_i*feats_cols)+3] = ""; //name string
					featsArrayOut[(feat_i*feats_cols)+4] = ""; //addr string
					
					//concatenate all tags into a string, while separating important tags like name/addr:
					bool tagsDone = false;
					while(!tagsDone) {
						getline(dataFileIn, currentLine);
						fileLine += 1;
						while(((currentLine.front()==' ') || (currentLine.front()=='\t')) && (currentLine.size() > 1)) { //trim any tabulation
							currentLine.erase(0, 1);
						}
						if(currentLine.find("\": ") != string::npos) { //remove space between key and value (makes strings a bit shorter)
							currentLine.erase(currentLine.find(" "), 1);
						}
						
						if(currentLine == "}") {
							tagsDone = true;
						}
						else if(currentLine.find("\"name\":") == 0) {
							featsArrayOut[(feat_i*feats_cols)+3] = currentLine.substr(8,currentLine.find("\"",8)-8);
						}
						else if(currentLine.find("\"addr:") == 0) {
							featsArrayOut[(feat_i*feats_cols)+4] = featsArrayOut[(feat_i*feats_cols)+4] + currentLine;
						}
						else {
							featsArrayOut[(feat_i*feats_cols)+2] = featsArrayOut[(feat_i*feats_cols)+2] + currentLine;
						}
					}
				//	cout << featsArrayOut[(feat_i*feats_cols)+2] << endl;
					feat_i += 1;
				}
				else if((currentLine == "},") || (dataFileIn.eof())) {
					//reached end of node fields
					if(fieldsFound < 3) {
						cout << "Unable to parse JSON. Unexpected end of node at line " << fileLine << " after only " << fieldsFound << " fields.\n";
						exit(1);
					}
					else {
						thisObjDone = true;
					}
				}
			}
			node_i += 1;
		}
		else if(currentLine == "\"type\": \"way\",") {
			//This brach has same structure as node parsing above, but is looking for different keys: tags is mandatory here
		
		//	cout << "Found a way type feature on line " << fileLine << endl;
			wayCount += 1;
			
			int fieldsFound = 0;
			bool thisObjDone = false;
			while(!thisObjDone) {
				getline(dataFileIn, currentLine);
				fileLine += 1;
				while(((currentLine.front()==' ') || (currentLine.front()=='\t')) && (currentLine.size() > 1)) { //trim any tabulation
					currentLine.erase(0, 1);
				}
			//	cout << fileLine << "\t" << currentLine << endl;
				
				if(currentLine.substr(0,5) == "\"id\":") {
					fieldsFound += 1;
					if(currentLine.back()==',') { currentLine.pop_back(); }
					featsArrayOut[(feat_i*feats_cols)+0] = currentLine.substr(6); //from 5 to rest of string
				}
				else if(currentLine == "\"nodes\": [") {
					fieldsFound += 1;
					
					featsArrayOut[(feat_i*feats_cols)+1] = ""; //initialize node string to empty
					
					//concatenate all nodes into a string: (same code block as tags)
					bool tagsDone = false;
					while(!tagsDone) {
						getline(dataFileIn, currentLine);
						fileLine += 1;
						while(((currentLine.front()==' ') || (currentLine.front()=='\t')) && (currentLine.size() > 1)) { //trim any tabulation
							currentLine.erase(0, 1);
						}
						
						if(currentLine.front() == ']') {
							tagsDone = true;
						}
						else {
							featsArrayOut[(feat_i*feats_cols)+1] = featsArrayOut[(feat_i*feats_cols)+1] + currentLine;
						}
					}
					
				}
				else if(currentLine == "\"tags\": {") {
					fieldsFound += 1;
					
					featsArrayOut[(feat_i*feats_cols)+2] = ""; //initialize tag string to empty
					featsArrayOut[(feat_i*feats_cols)+3] = ""; //name string
					featsArrayOut[(feat_i*feats_cols)+4] = ""; //addr string
					
					//concatenate all tags into a string:
					bool tagsDone = false;
					while(!tagsDone) {
						getline(dataFileIn, currentLine);
						fileLine += 1;
						while(((currentLine.front()==' ') || (currentLine.front()=='\t')) && (currentLine.size() > 1)) { //trim any tabulation
							currentLine.erase(0, 1);
						}
						if(currentLine.find("\": ") != string::npos) { //remove space between key and value (makes strings a bit shorter)
							currentLine.erase(currentLine.find(" "), 1);
						}
						
						if(currentLine == "}") {
							tagsDone = true;
						}
						else if(currentLine.find("\"name\":") == 0) {
							featsArrayOut[(feat_i*feats_cols)+3] = currentLine.substr(8,currentLine.find("\"",8)-8);
						}
						else if(currentLine.find("\"addr:") == 0) {
							featsArrayOut[(feat_i*feats_cols)+4] = featsArrayOut[(feat_i*feats_cols)+4] + currentLine;
						}
						else {
							featsArrayOut[(feat_i*feats_cols)+2] = featsArrayOut[(feat_i*feats_cols)+2] + currentLine;
						}
					}
				}
				else if((currentLine == "},") || (dataFileIn.eof())) {
					//reached end of feature fields
					if(fieldsFound < 3) {
						cout << "JSON Warning: Unexpected end of element keys at line " << fileLine << " after only " << fieldsFound << " fields. Probably a member of a relation.\n";
					//	exit(1);
						thisObjDone = true;
					}
					else {
						thisObjDone = true;
					}
				}
			}
			feat_i += 1;
		}
	}
	
	dataFileIn.close();
	cout << "Done parsing JSON file.\n";
	cout << "File has " << wayCount << " features of type 'way'.\n";
	
	//output exact size of data, array will actually be slightly bigger
	nodesSize = node_i;
	featsSize = feat_i;
	
	nodeArrayOut = nodesArrayOut; //TODO does not seem to be working as expected
	featArrayOut = featsArrayOut;
	
	return; // don't return anything, array pointers are written to vars passed-in by ref
	//length of arrays is already stored in variable passed-in by ref
}


#endif /* DATAFUNCTIONS_H */

