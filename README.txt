

Match plan:		
Run Overpass query		
Create table of nodes from returned JSON:		
	Take all nodes as-is	
	Take all Ways or relations, use first node in data as the coordinate for table. (Future: use average lat/lon of all nodes in way/relation)	
Step through master list (from city)		
	For each OSM entry: (other table)	
		Check if within 0.006 lat/long rect (about 450m lon in Delta)
		If yes, do fuzzy match on name
		Store score and index to master table
		If duplicates, alert user.
When done, must allow broswing table to review unmatched entries (low scores) for uploading.		


Wider automation challenges:		

Converting city categories to OSM tags
	Use NAICS data, need to make algorithm that properly generalizes.
Generating HTTP request with OSM login to send new node to OSM in a changeset.
	This may never be a good idea, always use ID editor for uploading, to get better sense of surrounding data.

Data from city: converting northing/easting to Lat/Lon		
	Recognize columns: all decimal numbers. Which is Lat/lon: I even did this incorrectly. Required trial and error.	
	Determine good diverse anchor points	
	Find about 10 anchors on OSM	
	Perform linear regression	
	Apply transformation and save to CSV	
	Check quality when searching for nodes in matching stage.	
